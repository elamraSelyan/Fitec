import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object wordCount {

  def main(args: Array[String]) {
    //Create a SparkContext to initialize Spark

    val dateFormatter = new SimpleDateFormat("ddMMyyyy_HHmmss")
    var dateconverter = new Date()
    var jobFinishTime = dateFormatter.format(dateconverter)

    println("[1] Standalone")
    println("[2] Cluster")

    //val depMod = scala.io.StdIn.readLine("Please select your deploy mode > ")
    val depMod = "2"


    var conf = new SparkConf()
    var input: String = ""
    var output: String = ""
    val user = System.getProperty("user.name")

    depMod match {
      // Standalone mode
      case "1"  =>
        conf.setAppName("Word count")
        conf.setMaster("local[*]")
        input = "../Spark_dataSets/shakespeare.txt"
        output = "/tmp/"+user+"_shakespeareWordCount_"+jobFinishTime

      // Cluster mode
      case "2"  =>
        conf.setAppName("Word count")
        input = "hdfs:///tmp/shakespeare.txt"
        output = "hdfs:///tmp/"+user+"_shakespeareWordCount_"+jobFinishTime

      case whoa  => println("Unexpected case: " + whoa.toString)
    }

    val sc = new SparkContext(conf)
    val textFile = sc.textFile(input) // Standalone mode

    //word count
    val counts = textFile.flatMap(line => line.split(" "))
      .map(word => (word, 1))
      .reduceByKey(_ + _)

    counts.foreach(println)
    System.out.println("Total words: " + counts.count());


    counts.saveAsTextFile(output)
  }
}